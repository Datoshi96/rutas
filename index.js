//Se traen utilidades de js
const fs = require('fs');
const { promisify } = require('util');

//Constante para llamar la lectura del arhivo
const readFilePromise = promisify(fs.readFile);

//Método principal 
const executeMap = async mapFilePath => {

    const INDEX_MAPS = {
        VERTEX: {},
        EDGE_FROM: {}
    };

    
    //Leer el archivo y asignarlo a una variable
    const inputs = await readFilePromise(mapFilePath, { encoding: 'utf8' });
    //Separador de filas por salto de linea
    const inputRows = inputs.split('\n');
    // Se obtienen filas y columnas
    const [rows, cols] = inputRows.shift().split(' ').map(n => Number(n));
    // matriz del archivo
    const inputMap = inputRows.slice(0, rows).map(r => r.split(' ').map(n => Number(n)));
    const vertices = [];
    const edges = []; 

    for (let r = 0; r < rows; r++) {
        for (let c = 0; c < cols; c++) {
            const currentVertexId = `r${r}_c${c}`;
            const currentVertexValue = inputMap[r][c];

            // campos vecinos
            const north = (r > 0) ? { id: `r${r - 1}_c${c}`, value: inputMap[r - 1][c] } : undefined;
            const west = (c > 0) ? { id: `r${r}_c${c - 1}`, value: inputMap[r][c - 1] } : undefined;
            const east = (c < cols - 1) ? { id: `r${r}_c${c + 1}`, value: inputMap[r][c + 1] } : undefined;
            const south = (r < rows - 1) ? { id: `r${r + 1}_c${c}`, value: inputMap[r + 1][c] } : undefined;

            const definedNeighbors = [north, east, west, south].filter(n => typeof n !== 'undefined');
            const validPathsFromCurrentVertex = definedNeighbors.filter(n => n.value < currentVertexValue);
            const validPathsToCurrentVertex = definedNeighbors.filter(n => n.value >= currentVertexValue);

            // vertices
            vertices.push({
                id: currentVertexId,
                value: currentVertexValue,
                isSource: !validPathsToCurrentVertex.length
            });
            INDEX_MAPS.VERTEX[currentVertexId] = vertices.length - 1;
            INDEX_MAPS.EDGE_FROM[currentVertexId] = [];
            // bordes
            validPathsFromCurrentVertex.forEach(p => {
                edges.push({
                    from: currentVertexId,
                    to: p.id
                });
                INDEX_MAPS.EDGE_FROM[currentVertexId].push(edges.length - 1);
            })
        }
    }

    const sourceVertices = vertices.filter(v => v.isSource);

    //Máximas rutas
    const maxPaths = sourceVertices.map(v => getMaxPath(v, vertices, edges, INDEX_MAPS));

    // filtro rutas por mayor longitud
    let maxPathLen = 0;
    maxPaths.forEach(m => {
        maxPathLen = m.length > maxPathLen ? m.length : maxPathLen;
    });

    const maxPathsFilteredByLen = maxPaths.filter(m => m.length === maxPathLen)

    // filtro de caminos por caída máxima
    let maxDepth = 0;
    maxPathsFilteredByLen.forEach(m => {
        const start = m[0].value;
        const end = m[m.length - 1].value;
        const depth = start - end;
        maxDepth = depth > maxDepth ? depth : maxDepth;
    });
    const maxPathsFilteredByLenAndDepth = maxPathsFilteredByLen.filter(m => (m[0].value - m[m.length - 1].value) === maxDepth)
    //Imprimir resultado del camino mas eficiente
    console.log('///Resultado///');
    console.log(`Longitud de ruta: (${maxPathLen})`);
    console.log(`Caída de la ruta: (${maxDepth})`)
    maxPathsFilteredByLenAndDepth.map((maxPath) => {
        console.log('Ruta calculada: '+(maxPath.map(m => `${m.value}`).join('-')));
    })
}

//Método para obtener la ruta máxima
const getMaxPath = (parent, vertices, edges, INDEX_MAPS) => {
    const selectedEdges = INDEX_MAPS.EDGE_FROM[parent.id].map(eIdx => edges[eIdx]);
    const childNodes = selectedEdges.map(e => vertices[INDEX_MAPS.VERTEX[e.to]]);
    let maxPath = [parent];
    if (childNodes.length > 0) { 
        const maxPaths = childNodes.map(c => getMaxPath(c, vertices, edges, INDEX_MAPS));
        const maxLen = Math.max(...maxPaths.map(m => m.length));
        const filteredMaxPaths = maxPaths.filter(m => m.length === maxLen);

        if (filteredMaxPaths.length > 0) {
            const maxDepth = Math.max(...filteredMaxPaths.map(m => parent.value - m[m.length - 1].value));
            const m = filteredMaxPaths.find(m => (parent.value - m[m.length - 1].value) === maxDepth);
            maxPath = maxPath.concat(m);
        } else {
            maxPath = maxPath.concat(filteredMaxPaths[0]);
        }
    }
    return maxPath;
}



//llamado método de archivo 
executeMap('./map.txt');
